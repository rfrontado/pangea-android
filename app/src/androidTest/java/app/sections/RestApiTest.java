package app.sections;

import com.google.gson.JsonObject;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.List;

import app.data.foundation.dagger.DataModule;
import app.data.foundation.net.RestApi;
import app.domain.User;
import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
/**
 * Created by robertofrontado on 6/26/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RestApiTest {
    private RestApi restApiUT;

    @Before public void setUp() {
        restApiUT = new DataModule().provideRestApi();
    }

    @Test public void WhenGetUsersWithValidParamsThenGetUsers() {

        TestSubscriber<Response<List<User>>> subscriber = new TestSubscriber<>();
        restApiUT.getUsers(20, getPayload()).subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        Response<List<User>> users = subscriber.getOnNextEvents().get(0);

        assertThat(users.body().size(), is(not(0)));
        assertThat(subscriber.getOnNextEvents().size(), is(1));
    }

    @Test public void WhenGetUsersWithInvalidParamsThenGetError() {

        TestSubscriber<Response<List<User>>> subscriber = new TestSubscriber<>();
        restApiUT.getUsers(20, null).subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        assertThat(subscriber.getOnNextEvents().size(), is(0));
    }

    private JsonObject getPayload() {

        JsonObject id = new JsonObject();
        id.addProperty("type", "string");
        id.addProperty("ipsum", "id");

        JsonObject name = new JsonObject();
        name.addProperty("type", "string");
        name.addProperty("ipsum", "name");

        JsonObject avatar = new JsonObject();
        avatar.addProperty("type", "string");
        avatar.addProperty("ipsum", "small image");

        JsonObject properties = new JsonObject();
        properties.add("id", id);
        properties.add("name", name);
        properties.add("avatar", avatar);

        JsonObject payload = new JsonObject();
        payload.addProperty("type", "object");
        payload.add("properties", properties);

        return payload;
    }

}
