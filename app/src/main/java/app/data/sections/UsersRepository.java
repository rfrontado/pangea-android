package app.data.sections;

import com.google.gson.JsonObject;

import java.util.List;

import javax.inject.Inject;

import app.data.foundation.Repository;
import app.data.foundation.UIUtils;
import app.data.foundation.cache.RxProviders;
import app.data.foundation.net.RestApi;
import app.domain.User;
import rx.Observable;

/**
 * Created by robertofrontado on 6/24/16.
 */
public class UsersRepository extends Repository {

    final int USERS_COUNT = 20;

    @Inject
    public UsersRepository(RestApi restApi, RxProviders rxProviders, UIUtils uiUtils) {
        super(restApi, rxProviders, uiUtils);
    }

    public Observable<List<User>> getUsers() {
        return restApi.getUsers(USERS_COUNT, getPayload())
                .map(listResponse -> {
                    handleError(listResponse);
                    List<User> users = listResponse.body();
                    return users;
                });
    }

    private JsonObject getPayload() {

        JsonObject id = new JsonObject();
        id.addProperty("type", "string");
        id.addProperty("ipsum", "id");

        JsonObject name = new JsonObject();
        name.addProperty("type", "string");
        name.addProperty("ipsum", "name");

        JsonObject avatar = new JsonObject();
        avatar.addProperty("type", "string");
        avatar.addProperty("ipsum", "small image");

        JsonObject properties = new JsonObject();
        properties.add("id", id);
        properties.add("name", name);
        properties.add("avatar", avatar);

        JsonObject payload = new JsonObject();
        payload.addProperty("type", "object");
        payload.add("properties", properties);

        return payload;
    }
}
