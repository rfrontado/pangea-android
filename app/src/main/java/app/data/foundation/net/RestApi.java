/*
 * Copyright 2016 FuckBoilerplate
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.data.foundation.net;

import com.google.gson.JsonObject;

import java.util.List;

import app.domain.User;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Definition for Retrofit of every endpoint required by the Api.
 */
public interface RestApi {
    String URL_BASE = "http://schematic-ipsum.herokuapp.com";
    String CONTENT_TYPE = "Content-Type: application/json";

    @Headers(CONTENT_TYPE)
    @POST("/")
    public Observable<Response<List<User>>> getUsers(@Query("n") int n, @Body JsonObject payload);
}
