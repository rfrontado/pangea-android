package app.domain;

import lombok.Data;

/**
 * Created by robertofrontado on 6/24/16.
 */
@Data
public class User {

    private final String id;
    private final String name;
    private final String avatar;
    private boolean selected;

    // Using this because avatar is not an valid URL
    public String getAvatarUrl() {
        return "http://loremflickr.com/320/240/cats";
    }

}
