package app.presentation.sections.list;

import java.util.List;

import javax.inject.Inject;

import app.data.foundation.UIUtils;
import app.data.sections.UsersRepository;
import app.data.sections.WireframeRepository;
import app.domain.User;
import app.presentation.foundation.PresenterFragment;
import rx.Observable;

/**
 * Created by robertofrontado on 6/24/16.
 */
public class ListPresenter extends PresenterFragment {

    private final UsersRepository usersRepository;

    @Inject
    protected ListPresenter(WireframeRepository wireframeRepository, UIUtils uiUtils, UsersRepository usersRepository) {
        super(wireframeRepository, uiUtils);
        this.usersRepository = usersRepository;
    }

    Observable<List<User>> getUsers() {
        return usersRepository.getUsers();
    }
}
