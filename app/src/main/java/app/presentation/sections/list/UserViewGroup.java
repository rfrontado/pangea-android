package app.presentation.sections.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.base_app_android.R;

import app.domain.User;
import butterknife.Bind;
import butterknife.ButterKnife;
import library.recycler_view.OkRecyclerViewAdapter;

/**
 * Created by robertofrontado on 6/25/16.
 */
public class UserViewGroup extends FrameLayout implements OkRecyclerViewAdapter.Binder<User> {

    @Bind(R.id.iv_avatar) protected ImageView iv_avatar;
    @Bind(R.id.tv_name) protected TextView tv_name;

    public UserViewGroup(Context context) {
        super(context);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.user_view_group, this, true);
        ButterKnife.bind(this, view);
    }

    @Override
    public void bind(User user, int position) {

        tv_name.setText(user.getName());

        // Because the user.getAvatarUrl() doesn't provide a valid url...
        Picasso.with(getContext()).load(user.getAvatarUrl())
                .centerCrop()
                .fit()
                .into(iv_avatar);
    }
}
