package app.presentation.sections.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.base_app_android.R;

import app.domain.User;
import butterknife.Bind;
import butterknife.ButterKnife;
import library.recycler_view.OkRecyclerViewAdapter;

/**
 * Created by robertofrontado on 6/25/16.
 */
public class ButtonViewGroup extends FrameLayout implements OkRecyclerViewAdapter.Binder<User> {

    @Bind(R.id.btn_user) protected Button btn_user;

    public ButtonViewGroup(Context context) {
        super(context);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.button_view_group, this, true);
        ButterKnife.bind(this, view);
    }

    @Override
    public void bind(User user, int position) {
        btn_user.setSelected(user.isSelected());
        btn_user.setOnClickListener(v -> {
            // Keep the button state
            user.setSelected(!v.isSelected());
            v.setSelected(user.isSelected());
        });
    }
}
