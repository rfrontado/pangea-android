package app.presentation.sections.list;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import org.base_app_android.R;

import java.util.List;

import app.domain.User;
import app.presentation.foundation.views.BaseFragment;
import app.presentation.foundation.views.LayoutResFragment;
import butterknife.Bind;
import library.recycler_view.OkRecyclerViewAdapter;
import rx.Observable;

/**
 * A simple {@link Fragment} subclass.
 */

@LayoutResFragment(R.layout.fragment_list)
public class ListFragment extends BaseFragment<ListPresenter> {

    @Bind(R.id.rv_users) protected SuperRecyclerView rv_users;
    private OkRecyclerViewAdapter<User, ?> adapter;

    @Override
    protected void injectDagger() {
        getApplicationComponent().inject(this);
    }

    @Nullable
    @Override
    protected String getScreenNameForGoogleAnalytics() {
        return null;
    }

    @Override
    protected void initViews() {
        super.initViews();
        setUpRecyclerView();

        presenter.getUsers()
                .compose(safelyReportLoading())
                .subscribe(users -> populateViews(users));
    }

    private void setUpRecyclerView() {
        adapter = new ListAdapter();
        adapter.setOnItemClickListener((user, userViewGroup, position) -> {
            if (position % 2 == 0) {
                presenter.dataForNextScreen(user)
                        .compose(safelyReport())
                        .subscribe(_I -> wireframe.detail());
            } else {
                String message = "Position: " + position + " - Selected: " + user.isSelected();
                showSnackBar(Observable.just(message));
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_users.setLayoutManager(layoutManager);
        rv_users.setAdapter(adapter);
    }

    private void populateViews(List<User> users) {
        adapter.setAll(users);
    }

    class ListAdapter extends OkRecyclerViewAdapter {

        // These values are because an issue in the library (it's actually using 1 and 2), need to fix that
        private final int TYPE_USER = 3;
        private final int TYPE_BUTTON = 4;

        @Override
        protected View onCreateItemView(ViewGroup parent, int viewType) {
            if (viewType == TYPE_USER) {
                return new UserViewGroup(parent.getContext());
            }
            return new ButtonViewGroup(parent.getContext());
        }

        @Override
        public int getItemViewType(int position) {
            return position % 2 == 0 ? TYPE_USER : TYPE_BUTTON;
        }
    }

}
