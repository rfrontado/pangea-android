/*
 * Copyright 2016 FuckBoilerplate
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.presentation.sections;

import android.content.Intent;
import android.os.Bundle;

import org.base_app_android.R;

import javax.inject.Inject;

import app.data.sections.WireframeRepository;
import app.presentation.foundation.BaseApp;
import app.presentation.foundation.views.BaseActivity;
import app.presentation.foundation.views.SingleActivity;
import app.presentation.sections.detail.DetailFragment;
import app.presentation.sections.list.ListFragment;
import rx.Observable;

/**
 * Provides the routing for the application screens.
 */
public class Wireframe {
    private final BaseApp baseApp;

    @Inject public Wireframe(BaseApp baseApp) {
        this.baseApp = baseApp;
    }

    public void popCurrentScreen() {
        baseApp.getLiveActivity().finish();
    }

    public void list() {
        Bundle bundle = new Bundle();
        bundle.putString(BaseActivity.Behaviour.TITLE_KEY, baseApp.getString(R.string.users_list));
        bundle.putBoolean(BaseActivity.Behaviour.SHOW_BACK_KEY, false);
        bundle.putSerializable(BaseActivity.Behaviour.FRAGMENT_CLASS_KEY, ListFragment.class);

        Intent intent = new Intent(baseApp, SingleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtras(bundle);
        baseApp.getLiveActivity().startActivity(intent);
    }

    public void detail() {
        Bundle bundle = new Bundle();
        bundle.putString(BaseActivity.Behaviour.TITLE_KEY, baseApp.getString(R.string.user_detail));
        bundle.putSerializable(BaseActivity.Behaviour.FRAGMENT_CLASS_KEY, DetailFragment.class);

        Intent intent = new Intent(baseApp, SingleActivity.class);
        intent.putExtras(bundle);
        baseApp.getLiveActivity().startActivity(intent);
    }

}
