package app.presentation.sections.detail;

import java.util.List;

import javax.inject.Inject;

import app.data.foundation.UIUtils;
import app.data.sections.UsersRepository;
import app.data.sections.WireframeRepository;
import app.domain.User;
import app.presentation.foundation.PresenterFragment;
import rx.Observable;

/**
 * Created by robertofrontado on 6/24/16.
 */
public class DetailPresenter extends PresenterFragment {

    @Inject
    protected DetailPresenter(WireframeRepository wireframeRepository, UIUtils uiUtils) {
        super(wireframeRepository, uiUtils);
    }

    public Observable<User> getSelectedUser() {
        return wireframeRepository.getWireframeCurrentObject();
    }

}

