package app.presentation.sections.detail;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.base_app_android.R;

import app.presentation.foundation.views.BaseFragment;
import app.presentation.foundation.views.LayoutResFragment;
import butterknife.Bind;

/**
 * A simple {@link Fragment} subclass.
 */
@LayoutResFragment(R.layout.fragment_detail)
public class DetailFragment extends BaseFragment<DetailPresenter> {

    @Bind(R.id.tv_name) TextView tv_name;
    @Bind(R.id.iv_avatar) ImageView iv_avatar;

    @Override
    protected void injectDagger() {
        getApplicationComponent().inject(this);
    }

    @Nullable
    @Override
    protected String getScreenNameForGoogleAnalytics() {
        return null;
    }

    @Override
    protected void initViews() {
        super.initViews();

        presenter.getSelectedUser()
                .compose(safelyReport())
                .subscribe(user -> {
                   tv_name.setText(user.getName());

                    Picasso.with(getContext()).load(user.getAvatarUrl())
                            .centerCrop()
                            .fit()
                            .into(iv_avatar);
                });
    }
}
